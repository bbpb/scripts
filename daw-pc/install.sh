#!/bin/bash

DIR=$(realpath $(dirname $0))

if ! realpath ~/.bashrc | grep -q "$DIR" ; then
    mv ~/.bashrc /tmp
    ln -s $(realpath "$DIR/dotfiles/bashrc") ~/.bashrc
fi

if ! realpath ~/.vimrc | grep -q "$DIR" ; then
    rm ~/.vimrc 2>/dev/null
    ln -s $(realpath "$DIR/dotfiles/vimrc") ~/.vimrc
fi
