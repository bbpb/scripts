# scripts

A random collection of scripts we use for

- working with [ardour](https://ardour.org/).
- keeping dotfiles in sync
