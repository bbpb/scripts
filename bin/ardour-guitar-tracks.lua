#!/usr/bin/env ardour7-lua

local BUSSES = { "git", "git-special", "gitclean" }

local GROUPS = { "git-l", "git-r", "git-mono", "gitclean-l", "gitclean-r", }
local GROUPED_TRACKS = {
    ["git-l"] = { "git-l-direct", "git-l-sm57", "git-l-sennheiser", "git-l-rode", },
    ["git-r"] = { "git-r-direct", "git-r-sm57", "git-r-sennheiser", "git-r-rode", },
    ["git-mono"] = { "git-m-direct", "git-m-sm57", "git-m-sennheiser", "git-m-rode", },
    ["gitclean-l"] = { "gitclean-l-direct", "gitclean-l-sm57", "gitclean-l-sennheiser", "gitclean-l-rode", },
    ["gitclean-r"] = { "gitclean-r-direct", "gitclean-r-sm57", "gitclean-r-sennheiser", "gitclean-r-rode", },
}

local ROUTING = {
     ["git-m-direct"]     = "git-special",
     ["git-m-sm57"]       = "git-special",
     ["git-m-sennheiser"] = "git-special",
     ["git-m-rode"]       = "git-special",
}


--------------------------------------------------------------------------------

dofile(os.getenv("LUA_SCRIPTS").."/ardour-common.lua")

-- load session
open_session(arg[1])
local audio_engine = Session:engine()

-- setup tracks
for g = 1, #GROUPS do
    local routing_group = Session:new_route_group(GROUPS[g])
    for i = 1, #GROUPED_TRACKS[GROUPS[g]] do
        track_name = GROUPED_TRACKS[GROUPS[g]][i]
        if Session:route_by_name(track_name):isnil() then
            print("create track "..track_name)
            Session:new_audio_track(
                1,                          -- number of inputs
                2,                          -- number of outputs
                routing_group,
                1,                          -- number of tracks
                track_name,
                -1,                         -- sort order? (somehow)
                ARDOUR.TrackMode.Normal,
                true                        -- some magic?
            )
        end
    end
end

-- setup busses
for i = 1, #BUSSES do
    bus = BUSSES[i]
    if Session:route_by_name(bus):isnil() then
        print("create bus "..bus)
        inputs = 2
        Session:new_audio_route(
            inputs, 2, nil, 1, bus,
            ARDOUR.PresentationInfo.Flag.AudioBus,
            ARDOUR.PresentationInfo.max_order
        )
    end
end

-- route output
for i = 1, #BUSSES do
    route_output_of_track(BUSSES[i], audio_engine, ROUTING)
end
for group_name, tracks in pairs(GROUPED_TRACKS) do
    for i = 1, #tracks do
        route_output_of_track(tracks[i], audio_engine, ROUTING)
    end
end


pan({ "git-l-direct", "git-l-sm57", "git-l-sennheiser", "git-l-rode", }, 0)
pan({ "git-r-direct", "git-r-sm57", "git-r-sennheiser", "git-r-rode", }, 1)

-- lower room mic volume
minus_6_db = 0.501187
set_volume({ "git-l-rode", "git-r-rode", "git-m-rode", }, minus_6_db)

-- mute directs
mute({ "git-l-direct", "git-r-direct", "git-m-direct", })

Session:save_state("")
