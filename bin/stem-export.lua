#!/usr/bin/env ardour7-lua

file = arg[1]
directory = file:match("(.*/)")
basename = file:match("([^/]*)$"):gsub("%.ardour", "")

s = load_session(directory, basename)
assert(s)

-- for r in Session:get_routes():iter() do
--     if r:name() == "solo" then
--         print("found solo")
--     end
--     if r:active() then
--         print("track: "..r:name())
--     end
-- end

local rlp = ARDOUR.RouteListPtr()
-- local sel = Editor:get_selection()
-- local route = Session:route_by_name("solo")
local route = Session:route_by_name("dist-l")
print(route:name())
rlp:push_back(route)
print(rlp:size())

print("...")
print("")
-- print(Session:export_track_state(rlp, "/tmp/export"))
-- local exp = ARDOUR.SimpleExport
-- print("export "..exp)

--ARDOUR.SimpleExport:set_folder("/tmp/foo")
--ARDOUR.SimpleExport:set_name("test.wav")
print("all set")
print(ARDOUR.SimpleExport:run_export())

print("")
print("")
