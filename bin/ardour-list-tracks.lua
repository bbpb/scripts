#!/usr/bin/env ardour7-lua
-- https://discourse.ardour.org/t/controling-session-from-command-line/106678/3

dofile(os.getenv("LUA_SCRIPTS").."/ardour-common.lua")

open_session(arg[1])


for r in Session:get_routes():iter() do
    print("track "..r:name().."    - "..  r:input():n_ports():n_audio())
    for i = 1, r:input():n_ports():n_audio() do
        name = " - "
        port = r:input():nth(i-1)
        input = ""
        connected = ""
        if not port:isnil() then
            name = port:name()
            if port:receives_input() then
                input = "input!"
            end
            if port:connected() then
                connected = "connected!"
            end
        end
        print("    "..i.."  "..name .. "   | "..input.." "..connected)
    end
end
print(" - done - ")

e = Session:engine()
local _, t = e:get_backend_ports ("", ARDOUR.DataType("audio"), ARDOUR.PortFlags.IsOutput | ARDOUR.PortFlags.IsPhysical, C.StringVector())

print(t)


os.exit(0)

l = get_session_range()
if l then
    print("range: "..l:start():beats():str().." - "..l:_end():beats():str())
else
    print("no session range")
end
print()

function list_all_tracks()
    for r in Session:get_routes():iter() do
        if r:active() and not r:is_hidden() then
            track = r:to_track()
            total_regions = 0
            range_regions = 0

            if not track:isnil() then
                total_regions = track:playlist():region_list():size()
                if l then
                    range_regions = track:playlist()
                        :regions_touched(l:start(), l:_end())
                        :size()
                end
            end

            print("track: "..r:name().." - "..total_regions.." "..range_regions)
        end
    end
end

function list_regions_of_track(track_name)
    track = Session:route_by_name(track_name):to_track()
    total_regions = track:playlist():region_list():size()
    print("total_regions "..total_regions)

    if l then
        range_regions = track:playlist()
            :regions_touched(l:start(), l:_end())
            :size()
        print("range_regions "..range_regions)
    end

    for region in track:playlist():region_list():iter() do
        print("    region "..region:start():beats():str().." - "..region:length():beats():str())
    end

end

if arg[2] then
    list_regions_of_track(arg[2])
else
    list_all_tracks()
end
