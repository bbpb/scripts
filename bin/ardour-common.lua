
function setup_route(from, to, a)
    local src_outs = Session:route_by_name(from):n_outputs():n_audio()
    local to_ins = Session:route_by_name(to):n_inputs():n_audio()
    for i = 1, src_outs do
        n = i
        if src_outs > to_ins then n = 1 end
        from_port = from.."/audio_out "..i
        to_port = to.."/audio_in "..n
        print("setup_route "..from_port.." to "..to_port)
        a:get_port_by_name(from_port):disconnect_all()
        a:get_port_by_name(from_port):connect(to_port)
    end
end

function mute(tracks)
    -- TODO only one track gets muted if session is not saved afterwords!?
    Session:save_state("")
    for i = 1, #tracks do
        print("mute "..tracks[i])
        Session:route_by_name(tracks[i]):mute_control():set_value(1,1)
        Session:save_state("")
    end
end

function pan(tracks, value)
    for i = 1, #tracks do
        print("pan "..tracks[i].. " to "..value)
        Session:route_by_name(tracks[i])
            :pan_azimuth_control()
            :set_value(value, PBD.GroupControlDisposition.NoGroup)
    end
end

function set_volume(tracks, value)
    for i = 1, #tracks do
        print("set_volume "..tracks[i].. " to "..value)
        Session:route_by_name(tracks[i])
            :gain_control()
            :set_value(value, PBD.GroupControlDisposition.NoGroup)
    end
end

function delete_sends(srcn)
    local src = Session:route_by_name(srcn)
    for i = 0,5 do
        send = src:nth_send(i)
        if not send:isnil() then
            print("delete send "..send:name().." on track "..src:name())
            src:remove_processor(send, nil, true)
        end
    end
end

function setup_sidechain(srcn, dstn, dst_plugin_n)
    local src = Session:route_by_name(srcn)
    local dst = Session:route_by_name(dstn)
    local plugin = dst:nth_plugin(dst_plugin_n)
    if plugin:isnil() then
        return
    end

    local s = ARDOUR.LuaAPI.new_send(Session, src, src:amp())
    assert (not s:isnil())
    local send = s:to_send()
    send:set_remove_on_disconnect(true)

    local src_io = send:output():nth(0)
    local dst_io = plugin:to_plugininsert():sidechain_input():nth(0)

    print("connect send " .. src_io:name() .. " to " .. dst_io:name())
    src_io:connect(dst_io:name())
end

function get_session_range()
    for l in Session:locations():list():iter() do
        if l:name() == "session" then
            return l
        end
    end
end

function realpath(file)
    local handle = io.popen("realpath "..file)
    local result = handle:read("*a")
    handle:close()
    return result:gsub("%s+", "")
end

function open_session(filename)
    local file = realpath(filename)
    local directory = file:match("(.*/)")
    local basename = file:match("([^/]*)$"):gsub("%.ardour", "")
    local s = load_session(directory, basename)
    assert(s)
    return s
end

function route_output_of_track(name, a, routing)
    prefix = name:match("([^-]+)")
    if routing and routing[name] then
        setup_route(name, routing[name], a)
    elseif name:find("%-") and not Session:route_by_name(prefix):isnil() then
        setup_route(name, prefix, a)
    else
        setup_route(name, "Master", a)
    end
end
