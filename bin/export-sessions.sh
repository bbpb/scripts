#!/bin/bash

DIR="$RECORDING_DIR"
EXPORTER=$(which ardour7-export) || exit 1
TARGET="$DIR/export"
COVER_IMAGE="$TARGET/cover.jpg"
DRY_RUN=0

if [ ! -d "$TARGET" ] ; then
    echo "target dir does not exist: $TARGET"
    exit 1
fi


run() {
    echo $@
    [ $DRY_RUN -eq 0 ] && $@
}

export_session() {
    f="$(realpath $1)"
    if [ ! -f "$f" ] ; then
        echo "not a session file: $f"
        exit 1
    fi
    session_name=$(basename "$f" | sed -e 's/\.ardour$//')
    name=$(basename "$f" | sed -e 's/-[^-]*\.ardour$//')
    echo "$session_name -> $name"
    echo

    run "$EXPORTER" \
        -o "$TARGET/$name.wav" \
        "$(dirname $f)" "$session_name"

    run lame \
        --alt-preset insane \
        "$TARGET/$name.wav" "$TARGET/$name.mp3"

    run id3v2 \
        --artist    "bbpb" \
        --album     "247sm_$(date +%F)" \
        --genre     "Punk" \
        --year      "2024" \
        --song      "$(echo $name | cut -f2 -d\-)" \
        "$TARGET/$name.mp3"

    run eyeD3 --add-image "$COVER_IMAGE:FRONT_COVER" "$TARGET/$name.mp3"

    echo
}

if [ -n "$1" ]; then
    while [ -n "$1" ]; do
        export_session "$1"
        shift
    done
else
    echo exporting all songs
    for SONG in $(cat "$DIR/songs"); do
        echo $SONG
        LAST_SESSION="$(ls -rt $DIR/$SONG/*.ardour | tail -1)"
        export_session "$LAST_SESSION"
    done
fi
