#!/bin/bash

DIR="${RECORDING_DIR:-$PWD}"
BACKUP_DIR="$DIR/backups"

session_files() {
    ls $DIR/??-*/??-*.ardour
}

clean_old_duplicates() {
    for f in $(session_files); do
        last_checksum=""
        for b in $(ls $BACKUP_DIR/*$(basename $f) | sort); do
            cs=$(md5sum $b | awk '{print $1}')
            if [ "$cs" == "$last_checksum" ] ; then
                echo delete duplicate $b
                rm $b
            fi
            last_checksum="$cs"
        done
    done
}

backup_changes() {
    for f in $(session_files); do
        last_backup="$(ls $BACKUP_DIR/*-$(basename $f) | sort | tail -1)"
        if ! diff \
            <(md5sum "$f" | awk '{print $1}') \
            <(md5sum "$last_backup" | awk '{print $1}') \
            > /dev/null ; then
            echo "$f   changed -> $BACKUP_DIR/$(date +%F_%T)-$(basename $f)"
            cp "$f" "$BACKUP_DIR/$(date +%F_%T)-$(basename $f)"
        else
            echo "$f unchanged"
        fi
    done
}

if [ "$1" == "clean" ] ; then
    clean_old_duplicates
else
    backup_changes
fi
