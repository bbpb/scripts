#!/usr/bin/env ardour7-lua

local BUSSES = { "vocals" }

local GROUPS = {
"vocals-1",
"vocals-2",
"vocals-3",
"vocals-4",
"vocals-5",
"vocals-6",
"vocals-7",
"vocals-8",
}
local GROUPED_TRACKS = {
    ["vocals-1"] = { "vocals-1-foo", "vocals-1-bar" },
    ["vocals-2"] = { "vocals-2-foo", "vocals-2-bar" },
    ["vocals-3"] = { "vocals-3-foo", "vocals-3-bar" },
    ["vocals-4"] = { "vocals-4-foo", "vocals-4-bar" },
    ["vocals-5"] = { "vocals-5-foo", "vocals-5-bar" },
    ["vocals-6"] = { "vocals-6-foo", "vocals-6-bar" },
    ["vocals-7"] = { "vocals-7-foo", "vocals-7-bar" },
    ["vocals-8"] = { "vocals-8-foo", "vocals-8-bar" },
    ["vocals-9"] = { "vocals-9-foo", "vocals-9-bar" },
}

local ROUTING = {
}


--------------------------------------------------------------------------------

dofile(os.getenv("LUA_SCRIPTS").."/ardour-common.lua")

-- load session
open_session(arg[1])
local audio_engine = Session:engine()

-- setup tracks
for g = 1, #GROUPS do
    local routing_group = Session:new_route_group(GROUPS[g])
    for i = 1, #GROUPED_TRACKS[GROUPS[g]] do
        track_name = GROUPED_TRACKS[GROUPS[g]][i]
        if Session:route_by_name(track_name):isnil() then
            print("create track "..track_name)
            Session:new_audio_track(
                1,                          -- number of inputs
                2,                          -- number of outputs
                routing_group,
                1,                          -- number of tracks
                track_name,
                -1,                         -- sort order? (somehow)
                ARDOUR.TrackMode.Normal,
                true                        -- some magic?
            )
        end
    end
end

-- setup busses
for i = 1, #BUSSES do
    bus = BUSSES[i]
    if Session:route_by_name(bus):isnil() then
        print("create bus "..bus)
        inputs = 2
        Session:new_audio_route(
            inputs, 2, nil, 1, bus,
            ARDOUR.PresentationInfo.Flag.AudioBus,
            ARDOUR.PresentationInfo.max_order
        )
    end
end

-- route output
for i = 1, #BUSSES do
    route_output_of_track(BUSSES[i], audio_engine, ROUTING)
end
for group_name, tracks in pairs(GROUPED_TRACKS) do
    for i = 1, #tracks do
        route_output_of_track(tracks[i], audio_engine, ROUTING)
    end
end


Session:save_state("")
