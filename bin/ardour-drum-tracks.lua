#!/usr/bin/env ardour7-lua

local BUSSES = { "bd", "sd", "direct", "oh", "room", "drums" }

local GROUPED_TRACKS = {
    ["drums"] = {
        "bd-in", "bd-out", "sd-top", "sd-bot", "tom-1", "tom-2", "oh-l",
        "oh-r", "oh-hh", "oh-ride", "room-1", "room-2", "room-fx", "cowbell",
        "bd-sin",
    },
}

local ROUTING = {
    ["tom-1"]   = "direct",
    ["tom-2"]   = "direct",
    ["cowbell"] = "direct",
    bd          = "drums",
    sd          = "direct",
    direct      = "drums",
    oh          = "drums",
    room        = "drums",
}

--------------------------------------------------------------------------------

dofile(os.getenv("LUA_SCRIPTS").."/ardour-common.lua")

-- load session
open_session(arg[1])
local audio_engine = Session:engine()

-- setup busses
for i = 1, #BUSSES do
    bus = BUSSES[i]
    if Session:route_by_name(bus):isnil() then
        print("create bus "..bus)
        inputs = 2
        if (bus:find("bd") or bus:find("sd")) then
            inputs = 1
        end
        Session:new_audio_route(inputs, 2, nil, 1, bus, 1, 1)
    end
end

-- setup tracks
for group_name, tracks in pairs(GROUPED_TRACKS) do
    local routing_group = Session:new_route_group(group_name)
    for i = 1, #tracks do
        track_name = tracks[i]
        if Session:route_by_name(track_name):isnil() then
            print("create track "..track_name)
            Session:new_audio_track(
                1,                          -- number of inputs
                2,                          -- number of outputs
                routing_group,
                1,                          -- number of tracks
                track_name,
                -1,                         -- sort order? (somehow)
                ARDOUR.TrackMode.Normal,
                true                        -- some magic?
            )
        end
    end
end

-- route output
for i = 1, #BUSSES do
    route_output_of_track(BUSSES[i], audio_engine, ROUTING)
end
for group_name, tracks in pairs(GROUPED_TRACKS) do
    for i = 1, #tracks do
        route_output_of_track(tracks[i], audio_engine, ROUTING)
    end
end

-- reconnect sidechain sends
delete_sends("bd-in")
setup_sidechain("bd-in", "bd-sin", 1)

-- show tracks with regions, hide empty tracks
-- TODO Editor missing in headless mode
for t in Session:get_tracks():iter() do
    if t:to_track():playlist():n_regions() > 0 then
        print("show track "..t:name())
        -- Editor:show_track_in_display(Editor:rtav_from_route(t):to_timeaxisview(), false)
    else
        print("hide track "..t:name())
        -- Editor:hide_track_in_display(Editor:rtav_from_route(t):to_timeaxisview(), false)
        -- false -> dont scroll to track
    end
end

Session:save_state("")
