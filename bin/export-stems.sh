#!/bin/bash

if [ -z "$1" ] || [ ! -f "$1" ]; then
    exit 1
fi

session="$(realpath $1)"
FILE=$(basename "$session" | sed 's/\.ardour$//')
TMPFILE="$FILE-export"
DIRNAME=$(dirname "$session")

TARGET_DIR="$DIRNAME/stems"
if [ ! -d "$TARGET_DIR" ]; then
    mkdir -p "$TARGET_DIR"
fi

echo $DIRNAME
echo $FILE


get_tracks() {
cat <<EOF | ardour7-lua
s = load_session("$DIRNAME", "$FILE")
assert(s)
session_range = nil
for l in Session:locations():list():iter() do
    if l:name() == "session" then
        session_range = l
    end
end
for r in Session:get_routes():iter() do
    if r:active() and not r:is_hidden() then
        track = r:to_track()
        if not track:isnil() and not string.match(r:name(), " ") then
            if session_range then
                range_regions = track:playlist()
                    :regions_touched(session_range:start(), session_range:_end())
                    :size()
                if range_regions > 0 then
                    print("track:"..r:name())
                    --print("track:  "..range_regions)
                end
            end
        end
        --print("track:"..r:name())
    end
end

EOF

}

tracks="$(get_tracks 2>/dev/null | awk -F : '/^track:/{print $2}')"

echo "export tracks: "$tracks
echo

for TRACK in $tracks; do

    echo "exporting $TRACK   --   $DIRNAME/$TMPFILE.ardour"
    echo

cp "$session" "$DIRNAME/$TMPFILE.ardour"

cat <<EOF | ardour7-lua
s = load_session("$DIRNAME", "$TMPFILE")
assert(s)
local export_group = Session:new_route_group("export")
local others_group = Session:new_route_group("others")
local muteptr = ARDOUR.ControlListPtr()
local audio_engine = Session:engine()
for r in Session:get_routes():iter() do
    -- print("Track "..r:name())
    if r:name() == "$TRACK" then
        -- print("volume 0")
        r:gain_control():set_value(1.0, PBD.GroupControlDisposition.NoGroup)
        -- print("pan to mono")
        r:pan_azimuth_control():set_value(0.5, PBD.GroupControlDisposition.NoGroup)
        --export_group:add(r)
        -- print("keep relevant processors")
        i = 0
        while not r:nth_plugin(i):isnil() do
            local p = r:nth_plugin(i)
            -- print("deactivate plugin "..p:display_name())
            r:nth_plugin(i):deactivate()
            i = i + 1
        end
        -- print("route output of track to master")
        audio_engine:get_port_by_name(r:name().."/audio_out 1"):disconnect_all()
        audio_engine:get_port_by_name(r:name().."/audio_out 1"):connect("Master/audio_in 1")
        audio_engine:get_port_by_name(r:name().."/audio_out 2"):disconnect_all()
        audio_engine:get_port_by_name(r:name().."/audio_out 2"):connect("Master/audio_in 2")
    else
        if r:name() ~= "Master" then
            -- print("mute")
            muteptr:push_back(r:mute_control())
            audio_engine:get_port_by_name(r:name().."/audio_out 1"):disconnect_all()
            audio_engine:get_port_by_name(r:name().."/audio_out 2"):disconnect_all()
            -- print("disable processors")
            i = 0
            while not r:nth_plugin(i):isnil() do
                -- print("deactivate plugin "..r:nth_plugin(i):display_name())
                r:nth_plugin(i):deactivate()
                i = i + 1
            end
            --others_group:add(r)
        end
    end
    -- print("")
end
-- print("set_mute")
Session:set_controls(muteptr, 1, PBD.GroupControlDisposition.NoGroup)
-- print("saving")
Session:save_state("")
print("done")
EOF

echo
ardour7-export -o "$TARGET_DIR/$FILE-$TRACK.wav" "$DIRNAME" "$TMPFILE"

echo

done

rm "$DIRNAME/$TMPFILE.ardour"
