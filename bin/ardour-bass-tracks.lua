#!/usr/bin/env ardour7-lua

local BUSSES = { "bass" }

local GROUPS = {
"bass-1",
"bass-2",
"bass-3",
"bass-4",
"bass-5",
"bass-6",
"bass-7",
"bass-8",
}
local GROUPED_TRACKS = {
    ["bass-1"] = { "bass-1-direct", "bass-1-amp", "bass-1-mic", },
    ["bass-2"] = { "bass-2-direct", "bass-2-amp", "bass-2-mic", },
    ["bass-3"] = { "bass-3-direct", "bass-3-amp", "bass-3-mic", },
    ["bass-4"] = { "bass-4-direct", "bass-4-amp", "bass-4-mic", },
    ["bass-5"] = { "bass-5-direct", "bass-5-amp", "bass-5-mic", },
    ["bass-6"] = { "bass-6-direct", "bass-6-amp", "bass-6-mic", },
    ["bass-7"] = { "bass-7-direct", "bass-7-amp", "bass-7-mic", },
    ["bass-8"] = { "bass-8-direct", "bass-8-amp", "bass-8-mic", },
}

local ROUTING = {
}


--------------------------------------------------------------------------------

dofile(os.getenv("LUA_SCRIPTS").."/ardour-common.lua")

-- load session
open_session(arg[1])
local audio_engine = Session:engine()

-- setup tracks
for g = 1, #GROUPS do
    local routing_group = Session:new_route_group(GROUPS[g])
    for i = 1, #GROUPED_TRACKS[GROUPS[g]] do
        track_name = GROUPED_TRACKS[GROUPS[g]][i]
        if Session:route_by_name(track_name):isnil() then
            print("create track "..track_name)
            Session:new_audio_track(
                1,                          -- number of inputs
                2,                          -- number of outputs
                routing_group,
                1,                          -- number of tracks
                track_name,
                -1,                         -- sort order? (somehow)
                ARDOUR.TrackMode.Normal,
                true                        -- some magic?
            )
        end
    end
end

-- setup busses
for i = 1, #BUSSES do
    bus = BUSSES[i]
    if Session:route_by_name(bus):isnil() then
        print("create bus "..bus)
        inputs = 2
        Session:new_audio_route(
            inputs, 2, nil, 1, bus,
            ARDOUR.PresentationInfo.Flag.AudioBus,
            ARDOUR.PresentationInfo.max_order
        )
    end
end

-- route output
for i = 1, #BUSSES do
    route_output_of_track(BUSSES[i], audio_engine, ROUTING)
end
for group_name, tracks in pairs(GROUPED_TRACKS) do
    for i = 1, #tracks do
        route_output_of_track(tracks[i], audio_engine, ROUTING)
    end
end


Session:save_state("")
