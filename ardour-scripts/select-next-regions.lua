ardour {
    ["type"]    = "EditorAction",
    name        = "select next region",
}

function factory(unused_params)
    return function()
        selection = Editor:get_selection()
        new_selection = ArdourUI.SelectionList()

        for region in selection.regions:regionlist():iter() do
            boundary = region:playlist()
                :find_next_region_boundary(region:start():increment(), 1)
                :increment()

            next_region = region:playlist()
                :find_next_region(boundary, ARDOUR.RegionPoint.Start, 1)

            if not next_region:isnil() then
                new_selection
                    :push_back(Editor:regionview_from_region(next_region))
            end
        end

        Editor:set_selection(new_selection, ArdourUI.SelectionOp.Set)

        first_selected = Editor:get_selection().regions:regionlist():front()
        if not first_selected:isnil() then
            Editor:reset_x_origin(first_selected:start():samples())
        end
    end
end
